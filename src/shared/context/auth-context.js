import { createContext } from 'react';

export const AuthContext = createContext({
  isLoggedIn: false,
  iserId: null,
  token: null,
  login: () => {},
  logout: () => {},
});
